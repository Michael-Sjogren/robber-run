extends Node
var general_vol = 1.0
var music_volume = .75
var sfx_volume = .75
enum Bus {
	GENERAL = 0,
	SFX = 1,
	MUSIC = 2
}


func set_bus_volume( bus:int, vol:float):
	vol = clamp(vol , 0 , 1)
	AudioServer.set_bus_volume_db(bus, linear2db(vol))

func play_music():
	$music/AudioStreamPlayer.play(0)

func stop_music():
	$music/AudioStreamPlayer.stop()
