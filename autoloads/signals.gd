extends Node

signal give_player_coin(amount)
signal player_died()
signal start_game()
signal game_won()
signal toggle_pause()
signal reset_game()
