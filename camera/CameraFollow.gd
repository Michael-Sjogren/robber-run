extends Spatial


export (NodePath) var follow_target_path
export (float , 0  , 1000) var follow_speed
export (Vector3) var offset
var follow_target:Spatial = null
# Called when the node enters the scene tree for the first time.
func _ready():
	follow_target = get_node(follow_target_path)
	set_as_toplevel(true)


func _physics_process(delta):
	if follow_target:
		$Camera.transform.origin = offset
		global_transform.origin = global_transform.origin.linear_interpolate(follow_target.global_transform.origin ,follow_speed * delta)
	else:
		print(follow_target)
