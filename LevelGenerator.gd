extends Node

export (NodePath) onready var start = get_node(start) as Spatial
const PLAYER = preload("res://player/Player.tscn")
const END = preload("res://platforms/EndPlatform.tscn")
const platforms = [
	preload("res://platforms/GapSmallPlatform.tscn"),
	preload("res://platforms/ClimbPlatform.tscn"),
	preload("res://platforms/SlopedPlatformUpward.tscn"),
	preload("res://platforms/SlopedPlatformDownWard.tscn")
]
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	Signals.connect("reset_game",self , "reset_game")
	generate_level()

func generate_level():
	var prev_platform = start
	for i in range(100):
		var index = randi() % platforms.size()
		var spawn_point = prev_platform.get_node("End").global_transform.origin
		var platform = platforms[index].instance()
		add_child(platform)
		platform.global_transform.origin = spawn_point
		prev_platform = platform
	create_end(prev_platform)
	create_player()

func reset_game():
	get_tree().change_scene("res://Game.tscn")

func create_player():
	var player = PLAYER.instance()
	add_child(player)
	Globals.player = player
	player.global_transform.origin = start.get_node("SpawnPoint").global_transform.origin

func create_end(prev_platform):
	var end = END.instance()
	var spawn_point = prev_platform.get_node("End").global_transform.origin
	add_child(end)
	end.global_transform.origin = spawn_point

func _process(delta):
	pass
