extends ColorRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var music_slider:Slider = $CenterContainer/VBoxContainer/MusicVolume
onready var sfx_slider:Slider = $CenterContainer/VBoxContainer/SfxVolume
onready var general_slider:Slider = $CenterContainer/VBoxContainer/GeneralVolume

# Called when the node enters the scene tree for the first time.
func _ready():
	AudioManager.set_bus_volume(0, general_slider.value/100)
	AudioManager.set_bus_volume(1, sfx_slider.value/100)
	AudioManager.set_bus_volume(2, music_slider.value/100)


func _process(delta):
	pass


func _on_HideOptions_pressed():
	visible = false


func _on_GeneralVolume_value_changed(value):
	AudioManager.set_bus_volume(0,value/100)
	

func _on_SfxVolume_value_changed(value):
	AudioManager.set_bus_volume(1,value/100)


func _on_MusicVolume_value_changed(value):
	AudioManager.set_bus_volume(2,value/100)
