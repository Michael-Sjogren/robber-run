extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var is_pause_menu = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



func _process(delta):
	if Input.is_action_just_pressed("ui_cancel") and is_pause_menu:
		visible = !visible
		Signals.emit_signal("toggle_pause")

func _on_Play_pressed():
	get_tree().change_scene("res://Game.tscn")


func _on_Options_pressed():
	$Options.visible = true


func _on_Quit_pressed():
	get_tree().quit(0)


func _on_Resume_pressed():
	visible = false
	get_tree().paused = false
