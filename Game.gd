extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	Signals.connect("toggle_pause",self,"_on_toggle_pause")

func _on_toggle_pause():
	get_tree().paused = !get_tree().paused
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
