extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$VisibilityNotifier.connect("screen_exited",self,"_on_VisibilityNotifier_screen_exited")
	$VisibilityNotifier.connect("screen_entered",self,"_on_VisibilityNotifier_screen_entered")
	$DeathZone.connect("body_entered",self , "_on_DeathZone_body_entered")
	visible = false
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_DeathZone_body_entered(body):
	Signals.emit_signal("player_died")

func _on_VisibilityNotifier_screen_entered():
	visible = true
	print("hello")
func _on_VisibilityNotifier_screen_exited():
	if (Globals.player.global_transform.origin.dot($End.global_transform.origin)) > 0:
		queue_free()
