extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var count = 0

func _physics_process(delta):
	$Control/HBoxContainer2/Label2.text = str(get_parent().distance_traveled)
	pass
	
# Called when the node enters the scene tree for the first time.
func _ready():
	Signals.connect("give_player_coin", self , "update_coins")

func update_coins(amount:int):
	count += amount
	$Control/HBoxContainer/Label2.text = str(count)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
