extends KinematicBody


var velocity = Vector3.ZERO
var sliding = false
var disabled = true
var dead = false
var money:int = 0
export var move_speed = 20.0
export var max_jump_height = 2.0
export var min_jump_height = 1.0
export var jump_duration = 0.5

onready var gravity = 2 * max_jump_height / pow(jump_duration , 2)
onready var jump_vel = -sqrt(2 * gravity * max_jump_height)
#onready var state_machine = $AnimationTree.active
onready var start_pos = global_transform.origin
var distance_traveled = 0.0
func _ready():
	$AnimationTree.active = true
	Signals.connect("give_player_coin",self,"receive_coin")
	Signals.connect("player_died", self , "die")

func _gravity(delta:float):
	if not is_on_floor():
		velocity.y += -gravity * delta
	else:
		$AnimationTree.set("parameters/in_air/current",0)
func _jump(delta):
	velocity.y = -jump_vel
	move_and_slide_with_snap(Vector3.ZERO , Vector3.ZERO)
	$JumpSound.play(0)
	$AnimationTree.set("parameters/in_air/current",1)
	$AnimationTree.set("parameters/Jump/active",true)
	
func is_sliding():
	return sliding

func start():
	Signals.emit_signal("start_game")
	AudioManager.play_music()
	disabled = false	

func _slide():
	if $SlideTimer.time_left <= 0.0:
		$SlideTimer.start(1.45)
		$StandingCollider.disabled = true
		sliding = true
		$AnimationTree.set("parameters/Sliding/active",is_sliding())

func die():
	#Signals.show_game_over_panel()
	dead = true
	print(distance_traveled , " units" )
	print(money , " coins" )
	disabled = true
	visible = false
	$Hurt.play(0)
	yield(get_tree().create_timer(.5),"timeout")
	Signals.emit_signal("reset_game")
	AudioManager.stop_music()

func receive_coin(amount:int):
	$CoinPickup.play(0)
	money += amount

func _physics_process(delta):

	if disabled:
		if Input.is_action_just_pressed("jump"):
			start()
		return
	if dead:
		return
	if $RayFoward.is_colliding():
		pass
		#die()
	
	$AnimationTree.set("parameters/in_air_state/current",not $RayBot.is_colliding())
	_gravity(delta)
	if is_on_floor() or $RayBot.is_colliding():
		_run(delta)
	if Input.is_action_just_pressed("jump") and (is_on_floor() or $RayBot.is_colliding()) and not $RayTop.is_colliding():
		_jump(delta)
	if Input.is_action_pressed("slide") and not is_sliding() and (is_on_floor() or $RayBot.is_colliding()):
		_slide()
	if is_sliding():
		if $SlideTimer.is_stopped():
			if not $RayTop.is_colliding():
				$StandingCollider.disabled = false
				sliding = false
				$AnimationTree.set("parameters/Sliding/active",is_sliding())
	if is_on_floor() or $RayBot.is_colliding():
		velocity = move_and_slide_with_snap(velocity , -get_floor_normal() * .5 , Vector3.UP , true , 4 , rad2deg(45) , false)
	else:
		velocity = move_and_slide(velocity , Vector3.UP , false , 4 , rad2deg(45) , false)
	distance_traveled = round(start_pos.distance_to(global_transform.origin))
	
func _run(delta):
	 velocity.x = -move_speed * delta
