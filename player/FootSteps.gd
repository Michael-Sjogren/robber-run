extends AudioStreamPlayer3D


export ( Array , AudioStream  ) var variations = []


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func play_footsteps():
	var index = randi() % variations.size()
	var sfx:AudioStream = variations[index]
	var old_stream = stream
	
	stream = sfx
	stream.loop = false
	play()
	
